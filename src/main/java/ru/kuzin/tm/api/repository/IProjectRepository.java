package ru.kuzin.tm.api.repository;

import ru.kuzin.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    Project create(String userId, String name, String description);

    Project create(String userId, String name);

}