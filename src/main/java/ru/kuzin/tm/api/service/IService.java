package ru.kuzin.tm.api.service;

import ru.kuzin.tm.api.repository.IRepository;
import ru.kuzin.tm.enumerated.Sort;
import ru.kuzin.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    List<M> findAll(Sort sort);

}