package ru.kuzin.tm.api.service;

import ru.kuzin.tm.api.repository.IUserOwnedRepository;
import ru.kuzin.tm.enumerated.Sort;
import ru.kuzin.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

    List<M> findAll(String userId, Sort sort);

}