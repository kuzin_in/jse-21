package ru.kuzin.tm.command.user;

import ru.kuzin.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand{

    private final String NAME = "logout";

    private final String DESCRIPTION = "logout current user";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getAuthService().logout();
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}