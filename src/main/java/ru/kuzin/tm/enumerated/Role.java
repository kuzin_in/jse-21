package ru.kuzin.tm.enumerated;

public enum Role {

    USUAL("Usual user"),
    ADMIN("Administrator");

    private final String displayName;

    Role(final String displayName) {
        this.displayName =displayName;
    }

    public final String getDisplayName() {
        return displayName;
    }

}