package ru.kuzin.tm.exception.field;

public final class AccessDeniedException extends AbstractFieldException {

    public AccessDeniedException() {
        super("Error! You are not logged in. Please log in and try again...");
    }

}